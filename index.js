const express = require('express')
const path = require('path')
const fs = require('fs')
const net = require('net')
const open = require('opn')

const configJson = fs.readFileSync('./config.json')
const config = JSON.parse(configJson)

let BIND_IP = config.BIND_IP || '127.0.0.1'
let APP_PORT = config.APP_PORT || 8080
let WEB_HOSTNAME = config.WEB_HOSTNAME || 'localhost'
const LOCK_FILENAME = `${process.argv[1]}.lck`

function cleanup () {
  fs.unlinkSync(LOCK_FILENAME)
}

function exit () {
  process.exit()
}

function isPortTaken(port) {
  return new Promise(function(resolve, reject) {
    let tester = net.createServer()
    tester.once('error', function(err) {
      if (err.code === 'EADDRINUSE') resolve(true)
      else resolve(false)
    })
    tester.once('listening', function() {
      tester.close()
      resolve(false)
    })
	tester.listen(port, BIND_IP)
  })
}

function nextAvailablePort (port) {
  return isPortTaken(port).then(function(portTaken) {
    if (!portTaken) return Promise.resolve(port)
    else {
      let nextPort = port + 1
      if (nextPort > 65535) nextPort = 1025
      return nextAvailablePort (nextPort)
    }
  })
}

function takeLock () {
  process.stdin.resume()
  fs.writeFileSync(LOCK_FILENAME, APP_PORT)
  
  //do something when app is closing
  process.on('exit', cleanup);
  
  //catches ctrl+c event
  process.on('SIGINT', exit);
  
  // catches "kill pid" (for example: nodemon restart)
  process.on('SIGUSR1', exit);
  process.on('SIGUSR2', exit);
  
  //catches uncaught exceptions
  process.on('uncaughtException', exit);
}

function startServer () {
  const app = express()
    
  app.use(express.static('web'));
  
  app.get('*', function(req, res) {
      res.sendFile(path.resolve('web', 'index.html'));
  });
  
  app.listen(APP_PORT, BIND_IP, function() {
    console.log(`Listening on port ${APP_PORT}`)
    openWeb()
  })
}

function openWeb () {
  open(`http://${WEB_HOSTNAME}:${APP_PORT}/`)
}

isPortTaken(APP_PORT).then(function(portTaken) {
  let isLocked = fs.existsSync(LOCK_FILENAME)
  
  if (!portTaken && !isLocked) {
    // Everything clear, normal startup
    takeLock()
	startServer()
  } else if (!portTaken && isLocked) {
    // previous lock exists but port is not taken, is the locked port is not default port ?
	let LOCK_PORT = parseInt(fs.readFileSync(LOCK_FILENAME)) || APP_PORT
	
    isPortTaken(LOCK_PORT).then(function(isLocked2) {
      if (isLocked2) {
        // locked port is still taken, reopen the web only
        APP_PORT = LOCK_PORT
		openWeb()
      } else {
        // locked port is now free, start server with default port
        takeLock()
		startServer()
      }
    })
  } else if (portTaken && !isLocked) {
    // port is taken but executable lock is not present, try changing to different port, and continue normal startup
    nextAvailablePort(APP_PORT).then(function(newPort) {
      APP_PORT = newPort
      takeLock()
      startServer()
    })
  } else if (portTaken && isLocked) {
    // port is taken and lock exists, just open the web, assume the previous instance is running now
    APP_PORT = parseInt(fs.readFileSync(LOCK_FILENAME)) || APP_PORT
    openWeb()
  }
})
