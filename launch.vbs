Dim WshShell
Dim WshProcEnv
Dim system_architecture
Dim process_architecture

Set WshShell =  CreateObject("WScript.Shell")
Set WshProcEnv = WshShell.Environment("Process")

process_architecture= WshProcEnv("PROCESSOR_ARCHITECTURE") 

If process_architecture = "x86" Then
    WScript.Echo "Running as a " & process_architecture & " process."
    WshShell.Run "node32 index.js", 0
ElseIf process_architecture = "AMD64" Then
    WScript.Echo "Running as a " & process_architecture & " process."
    WshShell.Run "node64 index.js", 0
Else
    MsgBox "This program can run on either x86 or AMD64 only, but your system is " & process_architecture & ".", vbOKOnly + vbExclamation, "Unknown architecture"
End If
